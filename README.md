shell
=====

This project is an application shell for buidling rich web apps.  

The seed contains a sample single page application and is preconfigured with javascript libraries like AngularJS, RequireJS.

The app just has few examples which shows how to use it.

## Table of contents

- [Quick start](#quick-start)
- [Live Demo](#live-demo)
- [License](#license)

## Quick start

- [Download the latest release](https://github.com/ideajoint/shell/archive/master.zip)
- Clone the repo: git clone https://github.com/ideajoint/shell.git
- Install with [NuGet](https://www.nuget.org): Install-Package xxxxxx

## Live Demo

- [Sample app](http://shellapp.azurewebsites.net)

## License

Code released under [the MIT license](LICENSE.txt). Docs released under [Creative Commons](LICENSE-DOCS.txt).
