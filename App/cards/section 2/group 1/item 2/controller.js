var controller = function (viewModel, sce) {
    viewModel.flag = {}

    viewModel.data = "Data from the controller";

    viewModel.didRaiseEvent1 = function () {
        viewModel.flag.event1Raised = true;
    }

    viewModel.didRaiseEvent2 = function () {
        viewModel.flag.event2Raised = true;
    }
}

shell.registerController(controller);