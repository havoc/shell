var deps = ['require', 'application', 'angular', 'underscore'];
define(deps, function (require, application, ng, _) {

    var $ = ng.element;

    var getTemplateUrl = function (componentName) {
        var componentsTemplatePath = '/app/framework/scripts/components/templates/';
        return componentsTemplatePath + componentName + '.html';
    }

    var notImplemented = function() {
        alert('Not implemented');
    }


    // Service

    application.service('shellSearchDialog', function () {
        var view = getTemplateUrl('shellSearchDialog');
        var controller = function ($scope, $mdDialog) {
            $scope.hide = function () {
                $mdDialog.hide();
            };
            $scope.cancel = function () {
                $mdDialog.cancel();
            };
            $scope.answer = function (answer) {
                $mdDialog.hide(answer);
            };
        }
        return {
            view: view,
            controller: controller
        }
    });

    application.service('shellComService', ['$rootScope', function ($rootScope) {
        var self = this;

        var state = self.state = {};

        $rootScope.state = $rootScope.state || {};

        // Private

        var listeners = self.listeners = {};

        var addCardToList = function (cardId) {
            $rootScope.cardList = $rootScope.cardList || [];
            $rootScope.cardList.push(cardId);
        }

        // API Methods

        self.openCard = function (cardId) {
            var data = {cardId: cardId};
            state.lastOpenedCard = data;
            addCardToList(cardId);
            if (listeners.openCard) listeners.openCard(data);
        }

        self.getCard = function (index) {
            return $rootScope.cardList[index];
        }

        self.deleteCard = function (index) {
            $rootScope.cardList.splice(index, 1);
        }

        self.listen = function (event, callback) {
            listeners[event] = callback;
        }

        self.getState = function (stateId) {
            return state[stateId];
        }

        self.pushContentBy = function (offset) {
            if (offset == 0) $rootScope.state.hubLevel = 0;
            $rootScope.contentTransform = 'translate3d(' + offset + 'px, 0, 0)';
        }

        require(['json!/app/cards/config.json'], function (config) {
            state.cardDefaults = config ? config.defaults || {} : {};
            state.startupCardList = config ? config.startupCards || [] : [];
            state.ready = true;
        });

        // Fetch data to bind the Hub menu
        require(['json!/app/hubs/nav.json'], function (hubConfig) {
            $rootScope.hubs = hubConfig ? hubConfig.hubs : [];
            var activeHubTitle = hubConfig ? hubConfig.defaults.activeHubTitle : "";
            var activeHub = _.findWhere($rootScope.hubs, {"title": activeHubTitle});
            $rootScope.hubs.activeItem = activeHub ?
                activeHub
                : ($rootScope.hubs.length > 0 ? $rootScope.hubs[0] : {});
        });

        return {
            openCard: self.openCard,
            getCard: self.getCard,
            deleteCard: self.deleteCard,
            listen: self.listen,
            getState: self.getState,
            pushContentBy: self.pushContentBy
        }
    }]);

    // Factory

    application.factory('$shellBind', ['$interpolate', function ($interpolate) {
        return function (scope, attrs, bindings, defaults) {
            defaults = defaults || {};
            var scopeToBe = _.object(bindings || []);
            _.each(scopeToBe, function (value, key) {
                if (scope[key] === undefined) {
                    scope[key] = attrs[key] === undefined ? defaults[key] : attrs[key];
                }
            });
        }
    }]);

    // Directives

    application.directive('includeReplace', function () {
        return {
            restrict: 'A',
            link: function (scope, el, attrs) {
                el.replaceWith(el.children());
            }
        };
    });

    application.directive('shellApp', function () {
        return {
            restrict: 'E',
            replace: true,
            scope: false,
            transclude: true,
            templateUrl: getTemplateUrl('shellApp')
        };
    });

    application.directive('shellStopClick', function () {
        return {
            restrict: 'A',
            link: function (scope, element, attr) {
                element.bind('click', function (e) {
                    e.stopPropagation();
                });
            }
        };
    });

    application.directive('shellSideMenu', ['$timeout', '$rootScope', '$mdDialog', 'shellComService', 'shellSearchDialog', '$http',
        function ($timeout, $rootScope, $mdDialog, shellComService, shellSearchDialog, $http) {
            return {
                restrict: 'E',
                replace: true,
                link: function ($scope, $element, $attrs, ctrl) {
                    $scope.state = {
                        hubLevel: 0
                    };

                    $scope.didMenuItemClick = function (level, key, value) {
                        $scope.menuSelection = $scope.menuSelection || [];
                        $scope.menuSelection[level] = key;
                        if (!value.cardId) {
                            prepareMenuData(level + 1, key);
                            $scope.didMenuToggleClick(level + 1, true);
                        } else {
                            openCard(value.cardId);
                        }
                    }

                    function prepareMenuData(level, filter) {
                        $scope.menuList = $scope.menuList || [];
                        if (level == 1) {
                            $scope.menuList[level] = $scope.menuList[level] || $rootScope.cardTree;
                        } else {
                            $scope.menuList[level] = $scope.menuList[level - 1][filter];
                        }
                    }

                    function openDialog(dialogId) {
                        // TODO: Deduce controller and view dynamically from the dialogId
                        $mdDialog.show({
                            controller: shellSearchDialog.controller,
                            templateUrl: shellSearchDialog.view,
                            clickOutsideToClose: true,
                            targetEvent: event
                        })
                            .then(function (answer) {
                                console.log(answer);
                            }, function () {
                                console.log(answer);
                            });
                    }

                    function showList(dataUrl) {
                        $http.get(dataUrl).then(
                            function (result) {
                                if (result.status == 200) {
                                    $scope.hubList = result.data;
                                }
                            },
                            function (errorResult) {
                                console.debug(errorResult);
                            }
                        );
                        $scope.didMenuToggleClick(1, true);
                    }

                    function openHubPane(paneId) {
                        notImplemented();
                    }

                    function handleHubItemSelection(event, hubItem) {
                        switch (hubItem.actionType) {
                            case 'openCard' :
                                $scope.didMenuToggleClick(1, false);
                                openCard(hubItem.cardId);
                                break;
                            case 'openDialog':
                                $scope.didMenuToggleClick(1, false);
                                openDialog(hubItem.dialogId);
                                break;
                            case 'showList':
                                showList(hubItem.dataUrl);
                                break;
                            case 'openHubPane':
                                openHubPane(hubItem.paneId);
                                break;
                            case 'default':
                                console.debug('Unsupported action type :' + hubItem.actionType );
                                break;
                        }
                    }

                    $scope.didHubItemClick = function (event, hubItem) {
                        $rootScope.hubs.activeItem.active = false;
                        hubItem.active = true;
                        $rootScope.hubs.activeItem = hubItem;
                        handleHubItemSelection(event, hubItem);
                    }

                    $scope.didLogoClick = function () {
                        // var isHubPaneOpen = $scope.state.hubLevel >= 1;
                        $scope.didMenuToggleClick(1, false);
                    }

                    $scope.didMenuToggleClick = function (level, open) {
                        if (open) {
                            $scope.state.hubLevel = level;
                            if (level == 1) prepareMenuData(1);
                            $scope.menuList[level + 1] = [];
                        } else {
                            $scope.state.hubLevel = level - 1;
                            $timeout(function () {
                                $scope.$apply(function () {
                                    if (level != 1) { // Don't delete level 1 data, since it will not changed once it is loaded
                                        $scope.menuList[level] = [];
                                    }
                                });
                            }, 300);

                        }
                        var offset = 300 + ($scope.state.hubLevel - 1) * 40;
                        if (!open && $scope.state.hubLevel == 0) offset = 0;
                        setTransform(offset);
                    }

                    var openCard = function (cardId) {
                        $scope.selectedCardId = cardId;
                        shellComService.openCard(cardId);
                    }

                    var setTransform = function (offset) {
                        shellComService.pushContentBy(offset);
                    }

                    setTransform(0);
                },
                templateUrl: getTemplateUrl('shellSideMenu')
            };
        }])
    ;

    application.directive('shellCardArray', ['$timeout', '$rootScope', 'shellComService', '$ionicScrollDelegate', function ($timeout, $rootScope, shellComService, $ionicScrollDelegate) {
        return {
            restrict: 'E',
            replace: false,
            transclude: true,
            scope: {
                focusedMode: '=?'
            },
            controller: function ($scope, $element, $attrs) {
                var ctrl = this;
                var cardList = ctrl.cardList = $scope.cardList = $scope.cardList || [];
                var fixedSideMenu = 85;
                var effectiveScreenWidth = window.screen.width - fixedSideMenu;

                ctrl.cardCounter = 0;
                ctrl.completedCardCounter = 0;

                ctrl.scrolling = $scope.scrolling = false;
                ctrl.focusedMode = $scope.focusedMode = $scope.focusedMode || false;

                var sizeArray = ctrl.sizeArray = {
                    'strip': 100,
                    'narrow': Math.round(effectiveScreenWidth / 4),
                    'wide': effectiveScreenWidth
                }

                ctrl.addCard = function (card) {
                    var cardOffset = 0;
                    var cardWidth = sizeArray[card.cardSize];
                    var cardIndex = cardList.length;

                    if (cardList.length != 0) {
                        // Has cards
                        var lastCard = cardList.last();
                        cardOffset = lastCard.offset + lastCard.width;
                    }
                    // Set offser and width as card properties
                    card.cardIndex = cardIndex;
                    card.offset = cardOffset;
                    card.width = cardWidth;

                    // Add an item to cardList
                    cardList.push(card);
                    console.log('cardList', cardList);

                    // Scroll to the new cards offset
                    scrollTo(cardOffset);
                }

                function adjustFollowingCardsOffet(afterCardIndex, widthDelta) {
                    for (var i = afterCardIndex + 1; i < cardList.length; i++) {
                        cardList[i].offset += widthDelta;
                    }
                }

                ctrl.resizeCard = function (card, size) {
                    var index = card.cardIndex;
                    var newWidth = sizeArray[size];
                    var oldWidth = card.width;
                    var widthDelta = newWidth - oldWidth;

                    card.width = newWidth;
                    card.width = newWidth;
                    card.cardWidth = newWidth + 'px';
                    card.cardSize = size;

                    adjustFollowingCardsOffet(index, widthDelta);

                    console.log('cardList', cardList);
                    scrollTo(card.offset);
                }

                function scrollTo(offset) {
                    console.log('scroll to ' + offset);
                    if (ctrl.scroller) {
                        var position = ctrl.scroller.getScrollPosition();
                        $timeout(function () {
                            ctrl.scroller.scrollTo(offset, position.top, true);
                        }, 450);
                    }
                }

                ctrl.deleteCard = function (card) {
                    var index = card.cardIndex;
                    cardList.splice(index, 1);
                    shellComService.deleteCard(index); // Cascade delete to viewmodel

                    var widthDelta = card.width;
                    adjustFollowingCardsOffet(index - 1, -widthDelta);

                    if (cardList.length == 0) {
                        // All cards are removed, reset focus mode
                        $scope.focusedMode = false;
                    } else {
                        // Move to previous card
                        if (index != 0) {
                            scrollTo(cardList[index - 1].offset);
                        }
                    }
                }

                ctrl.focusCard = function (card, value) {
                    value = value || false;

                    if (ctrl.focusedCard) {
                        ctrl.focusedCard.focused = false;
                    }

                    card.focused = value;

                    if (card.focused || false) {
                        ctrl.focusedCard = card;
                        $scope.focusedMode = ctrl.focusedMode = true;
                    } else {
                        ctrl.focusedCard = false;
                        $scope.focusedMode = ctrl.focusedMode = false;
                    }
                }

                ctrl.getFocuseMode = function () {
                    return $scope.focusedMode;
                }

                Array.prototype.last = function () {
                    return this[this.length - 1];
                }

            },
            link: function ($scope, $element, $attrs, ctrl) {
                $rootScope.cardList = shellComService.getState('startupCardList');
                ctrl.scroller = $ionicScrollDelegate.$getByHandle('shellCardArrayScroll');
            },
            templateUrl: getTemplateUrl('shellCardArray')
        }
    }]);

    application.directive('shellCard', ['$shellBind', '$timeout', 'shellComService', function ($shellBind, $timeout, shellComService) {
        return {
            restrict: 'E',
            require: '^shellCardArray',
            replace: true,
            transclude: true,
            scope: {
                cardSize: '=?',
                cardTitle: '=?',
                cardControls: '=?',
                cardIndex: '=?',
                cardHasStripview: '=?',
                cardHasNarrowview: '=?',
                cardHasWideview: '=?',
                cardId: '=',
                showHeader: '=?',
                showControls: '=?'
            },
            link: function ($scope, $element, $attrs, ctrl) {
                var defaults = {
                    cardSize: 'narrow',
                    cardHasNarrowview: true,
                    cardHasWideview: true,
                    cardHasStripview: true,
                    cardTitle: '[no title]',
                    showHeader: true,
                    showControls: true,
                    focused: false,
                    closable: true
                }

                var bindings = ['cardSize', 'cardTitle', 'cardHasStripview', 'cardHasNarrowview', 'cardHasWideview', 'showHeader', 'showControls', 'closable'];

                var setupAsDynamicCard = function () {
                    var resolvePath = function (id, assetName) {
                        var cardsPath = "/app/cards/";
                        var path = cardsPath + id.replace(new RegExp("\\.", "g"), '/');
                        path = assetName !== undefined ? path + "/" + assetName : path;
                        return encodeURIComponent(path.replace(new RegExp("//", "g"), '/'));
                    };

                    var configFilename = "config.json";
                    var controllerFilename = "controller.js";

                    $shellBind($scope, $attrs, ['cardSize'], defaults);

                    var cardConfigUrl = "json!" + resolvePath($scope.cardId, configFilename);
                    var cardControllerUrl = resolvePath($scope.cardId, controllerFilename);
                    var cardAssetsUrl = resolvePath($scope.cardId, "assets");
                    var cardViewUrl = resolvePath($scope.cardId, "view." + $scope.cardSize + ".html");
                    var testCardViewUrl = "text!" + cardViewUrl;
                    // Dynamically LOAD controller logic of the card using requirejs
                    // and EXTEND the base card controller logic with it.
                    require([cardConfigUrl, cardControllerUrl],
                        function onload(config, controller, viewExists) {

                            // Apply global card defaults
                            var globalCardDefaults = shellComService.getState('cardDefaults');
                            config = config || {};
                            _.defaults(config, globalCardDefaults);

                            // Consuming config
                            _.extend(defaults, config);
                            if ($scope.cardSize != config.cardSize) {
                                $scope.cardSize = config.cardSize;
                                cardViewUrl = resolvePath($scope.cardId, "view." + $scope.cardSize + ".html");
                                testCardViewUrl = "text!" + cardViewUrl;
                            }


                            // Check if the required view exists for the card
                            require([testCardViewUrl],
                                function onViewLoad() {

                                    // View found, setup scope and inject controller code
                                    $scope.$applyAsync(function () {
                                        $shellBind($scope, $attrs, bindings, defaults);
                                        // Setting up the view
                                        $scope.assetsUrl = cardAssetsUrl;
                                        $scope.cardViewUrl = cardViewUrl;
                                        prepareAndAddCardToList();
                                    });

                                    // Setting up the controller
                                    if (controller) {
                                        controller($scope); //_.extend($scope, controller($scope));
                                    }
                                },
                                function onViewError(err) {
                                    // Handle View not found error
                                    if (err.xhr.status == 404) {
                                        if (err.requireModules[0] == testCardViewUrl) {
                                            console.error('View not found for "' + $scope.cardId + '" at "' + err.xhr.responseURL + '"');
                                            $scope.$applyAsync(function () {
                                                // Setting up the view
                                                $scope.cardTitle = "[VIEW NOT FOUND]";
                                                $scope.errors = $scope.errors || {
                                                        viewNotFound: true
                                                    }
                                                $scope.cardSize = 'narrow';
                                                prepareAndAddCardToList();
                                                $scope.cardHasWideview = $scope.cardHasStripview = false;
                                            });
                                        }
                                    }
                                });
                        },
                        function onerror(err) {
                            console.error('View assets load failure.', err);
                            if (err.requireType) return;
                        });

                    $scope.changeViewTemplateOnResize = function (cardSize) {
                        $scope.cardViewUrl = resolvePath($scope.cardId, "view." + cardSize + ".html");
                    };
                };

                var setupAsStaticCard = function () {
                    $shellBind($scope, $attrs, bindings, defaults);
                    prepareAndAddCardToList();
                };

                var setupCommonOptions = function () {
                    $scope.cardWidth = ctrl.sizeArray[$scope.cardSize] + 'px';
                };

                var addEventListeners = function () {
                    $element[0].addEventListener('mousemove', function () {
                        if (ctrl.scrolling) return; // Don't focus cards on mousemove when scrolling
                        if (ctrl.focusedMode) {
                            if (!$scope.focused) {
                                $scope.$applyAsync(function () {
                                    ctrl.focusCard($scope.cardIndex, true);
                                });
                            }
                        }
                    });
                };

                var prepareAndAddCardToList = function () {
                    setupCommonOptions();

                    addEventListeners();

                    ctrl.addCard($scope);
                };

                $shellBind($scope, $attrs, ['cardId']);
                $scope.dynamicContent = $scope.cardId ? true : false;

                if ($scope.dynamicContent) {
                    setupAsDynamicCard();
                } else {
                    setupAsStaticCard();
                }

                var resizeCard = $scope.resizeCard = function (cardSize) {
                    ctrl.resizeCard($scope, cardSize);
                    if ($scope.cardId) {
                        $scope.changeViewTemplateOnResize(cardSize);
                    }
                }

                $scope.didExpandClick = function () {
                    $scope.$applyAsync(function () {
                        resizeCard('wide');
                    });
                }

                $scope.didCollapseClick = function () {
                    $scope.$applyAsync(function () {
                        resizeCard('narrow');
                    });
                }

                $scope.didMinimizeClick = function () {
                    $scope.$applyAsync(function () {
                        $scope.restoreSize = ng.copy($scope.cardSize);
                        resizeCard('strip');
                    });
                }

                $scope.didRestoreClick = function () {
                    $scope.$applyAsync(function () {
                        var restoreSize = ng.copy($scope.restoreSize);
                        resizeCard(restoreSize);
                    });
                }

                $scope.didCloseClick = function () {
                    ctrl.deleteCard($scope);

                    //$scope.$applyAsync(function () {
                    //    var cardIndex = $scope.cardIndex;
                    //    ctrl.deleteCard($scope);
                    //    //$element[0].remove();
                    //});
                }

                $scope.didFocusToggleClick = function () {
                    $scope.$applyAsync(function () {
                        var currentState = $scope.focused;
                        ctrl.focusCard($scope, !$scope.focused);
                        $scope.focused = !currentState;
                    });
                }
            },
            templateUrl: getTemplateUrl('shellCard')
        }
    }]);

// Overlays

    application.directive('shellCardArray2', [
        '$timeout', '$rootScope', 'shellComService', '$ionicScrollDelegate', function ($timeout, $rootScope, shellComService, $ionicScrollDelegate) {
            return {
                restrict: 'E',
                replace: false,
                transclude: true,
                scope: {
                    focusedMode: '=?',
                },
                controller: function ($scope, $element, $attrs) {
                }
            }
        }
    ]);

    application.directive('shellCard2', [
        '$shellBind', '$timeout', 'shellComService', function ($shellBind, $timeout, shellComService) {
            return {
                restrict: 'E',
                require: '^shellCardArray',
                replace: true,
                transclude: true,
                scope: {
                    cardSize: '=?',
                    cardTitle: '=?',
                    cardControls: '=?',
                    cardIndex: '=?',
                    cardHasStripview: '=?',
                    cardHasNarrowview: '=?',
                    cardHasWideview: '=?',
                    cardId: '=',
                    showHeader: '=?',
                    showControls: '=?'
                },
                link: function ($scope, $element, $attrs, ctrl) {
                }
            }
        }
    ]);

    console.log("[OK] - Loading component definitions");
})
;