var deps = ['require', 'application'];
define(deps, function (require, application) {

    var appconfig = application.config(['$stateProvider', '$urlRouterProvider', 'routeResolverProvider',
        function ($stateProvider, $urlRouterProvider, routeResolverProvider) {

            // Route Configuration
            var route = routeResolverProvider.route;

            $urlRouterProvider.otherwise('/');

            $stateProvider
                .state('start', route.resolve('/', 'app/framework/scripts/modules/start'))
                .state('home', route.resolve('/home', 'app/framework/scripts/modules/home'))
                .state('test', route.resolve('/test', 'app/framework/scripts/modules/test'));

        }
    ]);

});