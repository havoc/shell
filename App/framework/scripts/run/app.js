var deps = ['angular', 'angularRoute', 'ngMaterial', 'routeResolver', 'ionicAngular', 'lazy'];
define(deps, function (angular) {
    'use strict';

    var namespace = "app";

    // SHELL API Begins

    window.shell = {
        registerController: function (controller) {
            var deps = [];
            define(deps, function () {
                return function ($scope) {
                    return controller($scope);
                }
            });
        },
        scroller: FTScroller,
        data: Lazy
    };

    // SHELL API Ends

    var application = angular.module(namespace, ['ionic', 'ui.router', 'ngMaterial', 'ngTouch', 'routeResolverServices', 'ion-affix']);

    // Helper function to resolve relative paths to absolute paths
    application.resolveAssetUrl = function (ns, relativePath) {
        ns = ns.replace(new RegExp("\\.", "g"), '/');
        var path = ns + "/" + relativePath;
        return path.replace(new RegExp("//", "g"), '/');
    };

    function resolveModuleName(ns, moduleName, moduleType) {
        var parts = ns.split(".");
        moduleName = moduleName || moduleType;
        var result = (parts.length > 1) ? ns + moduleName : moduleName;
        return result;
    }

    application.resolveControllerName = function (ns, controllerName) {
        return resolveModuleName(ns, controllerName, "Controller");
    };

    application.resolveServiceName = function (ns, serviceName) {
        return resolveModuleName(ns, serviceName, "Service");
    };

    application.config(['$controllerProvider', '$compileProvider', '$filterProvider', '$provide',
        function ($controllerProvider, $compileProvider, $filterProvider, $provide) {

            // Preferred approach is conventions (over configurations)
            // If and only if required, allow setting the base directories for views and controllers
            // Change default views and controllers directory using the following:
            // routeResolverProvider.routeConfig.setBaseDirectories('/app/views', '/app/controllers');

            application._directive = application.directive;

            angular.extend(application, {
                controller: $controllerProvider.register,
                directive: $compileProvider.directive,
                filter: $filterProvider.register,
                factory: $provide.factory,
                service: $provide.service
            });
        }
    ]);
    console.log('[OK] - Configuring App - ' + namespace);
    return application;
});

