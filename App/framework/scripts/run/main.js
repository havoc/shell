﻿function printLogo() {
    // Generated using http://fsymbols.com/generators/tarty/
    var version = "0.0.3";
    console.log("█▀▀ █░░█ █▀▀ █░░ █░░       Fork me : https://github.com/ideajoint/shell");
    console.log("▀▀█ █▀▀█ █▀▀ █░░ █░░       Demo    : http://shellapp.azurewebsites.net");
    console.log("▀▀▀ ▀░░▀ ▀▀▀ ▀▀▀ ▀▀▀       Docs    : http://ideajoint.github.io/shell");
    console.log("              v" + version);
}
printLogo();

var isDebugMode = false;
require.config(
    {
        baseUrl: '',
        urlArgs: 'bust=v1' + (isDebugMode ? '.' + (new Date().getTime()) : ''),
        waitSeconds: 0,  //make sure it is enough to load all scripts

        paths: {
            'require': 'app/framework/libs/requirejs/2.1.14/require.min',
            'text': 'app/framework/libs/requirejs/text/2.0.12/text.min',
            'json': 'app/framework/libs/requirejs/json/0.4.0/json.min',
            '$': 'app/framework/libs/jquery/2.1.1/jquery-2.1.1.min',
            'angular': 'app/framework/libs/angularjs/1.3.15/angular.min',
            'angularAnimate': 'app/framework/libs/angularjs/1.3.15/angular-animate.min',
            'angularAria': 'app/framework/libs/angularjs/1.3.15/angular-aria.min',
            'angularSanitize': 'app/framework/libs/angularjs/1.3.15/angular-sanitize.min',
            'angularTouch': 'app/framework/libs/angularjs/1.3.15/angular-touch.min',
            'angularRoute': 'app/framework/libs/ui-router/0.2.11/angular-ui-router.min',
            'ionic': 'app/framework/libs/ionic/1.0.0-beta.13/js/ionic.mod.min',
            'ionicAngular': 'app/framework/libs/ionic/1.0.0-beta.13/js/ionic-angular.mod.min',
            'ion-affix': 'app/framework/libs/ion-affix/1.0.2/ion-affix',
            'domReady': 'app/framework/libs/requirejs/domReady/2.0.1/domReady.min',
            'routeResolver': 'app/framework/scripts/utils/routeResolver',
            'application': 'app/framework/scripts/run/app',
            'routeConfig': 'app/framework/scripts/config/routeConfig',
            'loader': 'app/framework/scripts/utils/loader',
            'underscore': 'app/framework/libs/lo-dash/2.4.1/lodash.underscore.min',
            'lazy': 'app/framework/libs/lazyjs/0.3.2/lazy.min',
            'ngMaterial': 'app/framework/libs/material/0.9.0/angular-material.min'
        },

        // Dependencies
        shim: {
            'angular': { 'exports': 'angular' },
            'angularAnimate': { deps: ['angular'] },
            'angularAria': { deps: ['angular'] },
            'angularSanitize': { deps: ['angular'] },
            'angularTouch': { deps: ['angular'] },
            'angularRoute': { deps: ['angular'] },
            'ngMaterial': { deps: ['angular', 'angularAnimate', 'angularAria'] },
            'ionic': { deps: ['angular'], exports: 'ionic' },
            'ionicAngular': { deps: ['angular', 'ionic', 'angularRoute', 'angularAnimate', 'angularSanitize'] },
            'ion-affix': { deps: ['ionic'] },
            'underscore': { 'exports': 'underscore' },
            'lazy': { 'exports': 'lazy' }
        },

        priority: [
            'angular'
        ]
    }
);

// Bootstrap App
// http://code.angularjs.org/1.2.1/docs/guide/bootstrap#overview_deferred-bootstrap
//window.name = 'NG_DEFER_BOOTSTRAP!';

console.group("SHELL Bootstrapping Sequence");

var deps = ['require', 'angular', 'application', 'routeConfig', 'loader', 'app/framework/scripts/run/controllers', 'angularAnimate', 'angularTouch', 'ngMaterial', 'ion-affix'];
define(deps, function (require, angular, application) {
    'use strict';
    var namespace = "app";
    require(['domReady!'], function (document) {
        try {
            angular.bootstrap(document, [namespace]);
        } catch (e) {
            console.error(e.stack || e.message || e);
        }
        require(['lazy']);
    });
    console.log('[OK] - Bootstrapping App -', namespace);
    console.groupEnd();
});
