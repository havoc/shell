﻿var deps = ['angular', 'application', './namespace', 'loader', '/app/framework/scripts/components/definitions.js'];
define(deps, function (angular, application, namespace, loader) {
    var controllerName = application.resolveControllerName(namespace);

    application.controller(controllerName, ['$scope', '$timeout', '$state', 'shellComService',
        function controller($scope, $timeout, $state, shellComService) {
            var isAppReady = false;
            
        }
    ]);

    console.log('[OK] - Loading controller - ' + controllerName);
});