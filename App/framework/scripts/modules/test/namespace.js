define([
    '../namespace'
], function (parentNamespace) {
    var namespace = "test";
    return parentNamespace + "." + namespace;
});