﻿var deps = ['angular', 'application', './namespace', 'loader', '/app/framework/scripts/components/definitions.js'];
define(deps, function (angular, application, namespace, loader) {
    var controllerName = application.resolveControllerName(namespace);

    application.controller(controllerName, ['$scope', '$timeout', '$state', 'shellComService',
        function controller($scope, $timeout, $state, shellComService) {
            var isAppReady = false;
            
            function _init() {
                var vm = {
                    appName: "shell",
                    counter: 0,
                };
                angular.extend($scope, vm);

                $timeout(function () {
                    _checkIfAppReady();
                }, 1000);
            }
            _init();

            function _checkIfAppReady() {
                $scope.counter++;
                if (isAppReady) {
                    $state.go('home');
                }
                else {
                    $timeout(function () {
                        isAppReady = shellComService.getState('ready');
                        _checkIfAppReady();
                    }, 1000);
                }
            }
        }
    ]);

    console.log('[OK] - Loading controller - ' + controllerName);
});