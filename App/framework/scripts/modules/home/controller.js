var deps = [
        'angular',
        'underscore',
        'application',
        './namespace',
        '/app/framework/scripts/components/definitions.js'
];
define(deps, function (ng, _, application, namespace) {
    var controllerName = application.resolveControllerName(namespace);

    application.controller(controllerName, ['$scope', '$window', '$timeout', '$state', 'shellComService',
        function controller($scope, $window, $timeout, $state, shellComService) {
            var ready = shellComService.getState('ready');
            if (!ready) {
                $state.go('start');
            } else {
                $scope.ready = true;
            }
        }
    ]);
    console.log('[OK] - Loading controller - ' + controllerName);
});