define([
    '../namespace'
], function (parentNamespace) {
    var namespace = "home";
    return parentNamespace + "." + namespace;
});